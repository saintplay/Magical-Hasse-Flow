﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magical_Hasse_Flow
{
    public partial class Host : Form
    {
        public Host()
        {
            InitializeComponent();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            Int64 x;
            if (Int64.TryParse(txtValor.Text, out x))
            {
                if (x == 0)
                {
                    MessageBox.Show("Definitavmente estas mal de la cabeza!!");
                    return;
                }
                if (x < 0)
                {
                    MessageBox.Show("Solo Numeros Positivos!!!");
                    return;
                }
                if (x > int.MaxValue)
                {
                    MessageBox.Show("Alguien quiere explotar su computadora...");
                    return;
                }

                int n = (int)x;

                Hasse2D hasse = new Hasse2D();

                hasse.dominio = new List<Nodo>();

                for (int i = 1; i <= (n + 1) / 2; i++)
                {
                    if (n % i == 0)
                    {
                        hasse.dominio.Add(new Nodo(i));
                    }
                }
                hasse.dominio.Add(new Nodo(n));

                hasse.goHasse();

                Grafico grafico = new Grafico(hasse);

                if (hasse.antisimetricas.Count != 0)
                {
                    grafico.hasse = hasse;
                    grafico.Show();
                    grafico.DibujarHasse(hasse, grafico.buffer.Graphics);
                    grafico.buffer.Render(grafico.graphics);
                    grafico.Flow.Enabled = true;
                }
                else
                    MessageBox.Show("El numero no puede formar un Hasse de divisibilidad, que se le va a hacer? asi es la vida....");
            }
            else
            {
                MessageBox.Show("Eso es un numero??");
            }

        }
    }
}
