﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Magical_Hasse_Flow
{
    public class ColorRelacion
    {
        public Color color { get; set; }
        public bool v1 { get; set;}
        public bool v2 { get; set; }
        public bool v3 { get; set; }
        public int velocidad { get; set; }

        public ColorRelacion(bool velocidad1, bool velocidad2, bool velocidad3, int red, int green, int blue)
        {
            color = Color.FromArgb(red, green, blue);
            v1 = velocidad1;
            v2 = velocidad2;
            v3 = velocidad3;
            velocidad = 2;
        }
        public void CambiarColor(Random pAleatorio)
        {
            color = Color.FromArgb(pAleatorio.Next(256), pAleatorio.Next(256), pAleatorio.Next(256));
        }
        public void VariarColor()
        {
            int r = color.R;
            int g = color.G;
            int b = color.B;

            if (v1)
            {
                r += velocidad;
                if (r >= 255)
                {
                    v1 = false;
                    r = 255;
                }     
            }
            else
            {
                r -= velocidad;

                if (r <= 0)
                {
                    v1 = true;
                    r = 0;
                }                
            }
            if (v2)
            {
                g += velocidad;
                if (g >= 255)
                {
                    v2 = false;
                    g = 255;
                }             
            }
            else
            {
                g -= velocidad;
                if (g <= 0)
                {
                    v2 = true;
                    g = 0;
                }                  
            }
            if (v3)
            {
                b += velocidad;
                if (b >= 255)
                {
                    v3 = false;
                    b = 255;
                }                   
            }
            else
            {
                b -= velocidad;
                if (b <= 0)
                {
                    v3 = true;
                    b = 0;
                }                    
            }

            color = Color.FromArgb(r, g, b);
        }
    }
}
