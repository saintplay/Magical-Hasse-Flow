﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magical_Hasse_Flow
{
    public enum TiposRelacion {Antisimetrica, Transitiva, Reflexiva, Null}
    public class Relacion2D
    {
        public Nodo a { get; set; }
        public Nodo b { get; set; }
        public ColorRelacion color { get; set; }
        public TiposRelacion tipo { get; set; }
        
        public Relacion2D(Nodo pA, Nodo pB, TiposRelacion pTipo)
        {
            a = pA;
            b = pB;
            tipo = pTipo;
        }
    }
}
