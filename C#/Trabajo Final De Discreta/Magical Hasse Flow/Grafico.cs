﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magical_Hasse_Flow
{
    public enum MouseToca { Vacio, Relacion, Nodo, Nada}
    public partial class Grafico : Form
    {
        private int con;
        public Graphics graphics { get; set; }
        public BufferedGraphics buffer { get; set; }
        public BufferedGraphicsContext context { get; set; }
        public Point MouseInicial { get; set; }
        public MouseToca objeto { get; set; }
        public Nodo nodo { get; set; }
        public bool mostrarNodos { get; set; }
        public bool FunColor { get; set; }
        public bool FullColor { get; set; }
        public Hasse2D hasse { get; set; }
        public Grafico(Hasse2D pHasse)
        {
            hasse = pHasse;
            InitializeComponent();
            for (int i = 0; i < hasse.dominio.Count; i++)
            {
                if (hasse.dominio[i].nivel != -1)
                {
                    hasse.dominio[i].onMovimientoLabel += MoverNodo;
                    this.Controls.Add(hasse.dominio[i].label);
                }
            }
            this.Controls.Add(panel);
            objeto = MouseToca.Nada;
            mostrarNodos = false;
            FunColor = true;
            FullColor = true;
        }

        public void DibujarHasse(Hasse2D pHasse, Graphics pGraphics)
        {
            pGraphics.Clear(Color.SkyBlue);
            Relacion2D r;
            int factorX;
            int factorY;

            factorX = ((int)pGraphics.VisibleClipBounds.Width) / (pHasse.ancho);
            factorY = ((int)pGraphics.VisibleClipBounds.Height) / (pHasse.alto + 1);


                for (int i = 0; i < pHasse.antisimetricas.Count; i++)
                {
                    r = pHasse.antisimetricas[i];

                    r.a.xScreen = r.a.x * factorX / 2 - r.a.xDiff;
                    r.a.yScreen = (int)pGraphics.VisibleClipBounds.Height - r.a.y * factorY - r.a.yDiff;

                    r.b.xScreen = r.b.x * factorX / 2 - r.b.xDiff;
                    r.b.yScreen = (int)pGraphics.VisibleClipBounds.Height - r.b.y * factorY - r.b.yDiff;

                    if (FullColor)
                    {
                        if (FunColor)
                        {
                            pHasse.antisimetricas[i].color.VariarColor();
                        }

                        pGraphics.DrawLine(new Pen(pHasse.antisimetricas[i].color.color, 5), new Point(r.a.xScreen, r.a.yScreen), new Point(r.b.xScreen, r.b.yScreen));
                    }
                    else
                        pGraphics.DrawLine(new Pen(Color.SteelBlue, 5), new Point(r.a.xScreen, r.a.yScreen), new Point(r.b.xScreen, r.b.yScreen));
                }
        }

        private void Grafico_Resize(object sender, EventArgs e)
        {
            panel.Size = this.ClientRectangle.Size;
            graphics = panel.CreateGraphics();
            context = BufferedGraphicsManager.Current;
            buffer = context.Allocate(graphics, panel.ClientRectangle);
            DibujarHasse(hasse, buffer.Graphics);
            buffer.Render(graphics);
            ActualizarLabels();
            buffer.Dispose();
        }
        private void panel_Paint(object sender, PaintEventArgs e)
        {
            graphics = panel.CreateGraphics();
            context = BufferedGraphicsManager.Current;
            buffer = context.Allocate(graphics, panel.ClientRectangle);
            DibujarHasse(hasse, buffer.Graphics);
            buffer.Render(graphics);
            buffer.Dispose();

           Relacion2D r;
            int factorX;
            int factorY;

            factorX = ((int)graphics.VisibleClipBounds.Width) / (hasse.ancho);
            factorY = ((int)graphics.VisibleClipBounds.Height) / (hasse.alto + 1);

            Color color = Color.SteelBlue;

            for (int i = 0; i < hasse.antisimetricas.Count; i++)
            {
                r = hasse.antisimetricas[i];

                r.a.xScreen = r.a.x * factorX / 2 - r.a.xDiff;
                r.a.yScreen = (int)graphics.VisibleClipBounds.Height - r.a.y * factorY - r.a.yDiff;

                r.b.xScreen = r.b.x * factorX / 2 - r.b.xDiff;
                r.b.yScreen = (int)graphics.VisibleClipBounds.Height - r.b.y * factorY - r.b.yDiff;
            }
        }

        public void ActualizarLabels()
        {
            for (int i = 0; i < hasse.dominio.Count; i++)
            {
                if (hasse.dominio[i].nivel != -1)
                {
                    hasse.dominio[i].label.Location = new Point(hasse.dominio[i].xScreen, hasse.dominio[i].yScreen);
                }
            }
        }
        public void ActualizarLabel(Nodo pNodo)
        {
            pNodo.label.Location = new Point(pNodo.xScreen, pNodo.yScreen);
        }

        private void Grafico_Load(object sender, EventArgs e)
        {
            graphics = panel.CreateGraphics();
            context = BufferedGraphicsManager.Current;
            buffer = context.Allocate(graphics, panel.ClientRectangle);
        }

        private void panel_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                objeto = MouseToca.Vacio;
                Point mousePoint = new Point(e.Location.X, e.Location.Y);
                Rectangle mousePointer = new Rectangle(e.Location.X - 6, e.Location.Y - 6, 12, 12);

                for (int i = 0; i < hasse.dominio.Count; i++)
                {                  
                    if (hasse.dominio[i].nivel != -1)
                    {
                        nodo = hasse.dominio[i];

                        if (nodo.xScreen >= mousePointer.Left && nodo.xScreen <= mousePointer.Right && nodo.yScreen >= mousePointer.Top && nodo.yScreen <= mousePointer.Bottom)
                        {
                            objeto = MouseToca.Nodo;
                            break;
                        } 
                    }
                }
                
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (mostrarNodos)
                {
                    for (int i = 0; i < hasse.dominio.Count; i++)
                    {
                        if (hasse.dominio[i].nivel != -1)
                        {
                            hasse.dominio[i].label.Visible = false;
                        }
                    }
                    mostrarNodos = false;
                }
                else
                {
                    for (int i = 0; i < hasse.dominio.Count; i++)
                    {
                        if (hasse.dominio[i].nivel != -1)
                        {
                            hasse.dominio[i].label.Location = new Point(hasse.dominio[i].xScreen, hasse.dominio[i].yScreen);
                            hasse.dominio[i].label.Visible = true;
                        }
                    }
                    mostrarNodos = true;
                }
            }
        }

        public void MoverNodo(Nodo pNodo, int posX, int posY)
        {
            pNodo.xDiff += pNodo.xScreen - posX;
            pNodo.yDiff += pNodo.yScreen - posY;
            graphics = panel.CreateGraphics();
            context = BufferedGraphicsManager.Current;
            buffer = context.Allocate(graphics, panel.ClientRectangle);
            DibujarHasse(hasse, buffer.Graphics);
            buffer.Render(graphics);
            ActualizarLabel(pNodo);
            buffer.Dispose();
        }

        private void panel_MouseMove(object sender, MouseEventArgs e)
        {
            goFunColor();

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (objeto == MouseToca.Nodo)
                {
                    MoverNodo(nodo, e.X, e.Y);
                }
                else if (objeto == MouseToca.Relacion)
                { 
                
                }
            }                  
        }

        private void panel_MouseUp(object sender, MouseEventArgs e)
        {
            objeto = MouseToca.Nada;
        }

        public void goFunColor()
        {
            if (FullColor)
            {
                Relacion2D r;
                graphics = panel.CreateGraphics();
                context = BufferedGraphicsManager.Current;
                buffer = context.Allocate(graphics, panel.ClientRectangle);
                buffer.Graphics.Clear(Color.SkyBlue);

                for (int i = 0; i < hasse.antisimetricas.Count; i++)
                {
                    r = hasse.antisimetricas[i];
                    hasse.antisimetricas[i].color.VariarColor();
                    buffer.Graphics.DrawLine(new Pen(hasse.antisimetricas[i].color.color, 5), new Point(r.a.xScreen, r.a.yScreen), new Point(r.b.xScreen, r.b.yScreen));
                }
                buffer.Render(graphics);
            }        
        }

        private void Flow_Tick(object sender, EventArgs e)
        {
            goFunColor();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            if (mostrarNodos)
            {
                for (int i = 0; i < hasse.dominio.Count; i++)
                {
                    if (hasse.dominio[i].nivel != -1)
                    {
                        hasse.dominio[i].label.Visible = false;
                    }
                }
                mostrarNodos = false;
            }
            else
            {
                for (int i = 0; i < hasse.dominio.Count; i++)
                {
                    if (hasse.dominio[i].nivel != -1)
                    {
                        hasse.dominio[i].label.Location = new Point(hasse.dominio[i].xScreen, hasse.dominio[i].yScreen);
                        hasse.dominio[i].label.Visible = true;
                    }
                }
                mostrarNodos = true;
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            if (Flow.Enabled == true)
            {
                Flow.Enabled = false;
                FullColor = false;
                graphics = panel.CreateGraphics();
                context = BufferedGraphicsManager.Current;
                buffer = context.Allocate(graphics, panel.ClientRectangle);
                DibujarHasse(hasse, buffer.Graphics);
                buffer.Render(graphics);
                ActualizarLabels();
                buffer.Dispose();
            }
            else
            {
                Flow.Enabled = true;
                FullColor = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Disponible para android...");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Intenta arrastrar los nodos(vertices) para formar Hasses Locos!!!");
        }
    }
}
