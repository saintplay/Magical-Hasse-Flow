﻿namespace Magical_Hasse_Flow
{
    partial class Control
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerar = new System.Windows.Forms.Button();
            this.txtbxDesde = new System.Windows.Forms.TextBox();
            this.txtbxHasta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbxNumero = new System.Windows.Forms.TextBox();
            this.btnRandom = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lstbxLista = new System.Windows.Forms.ListBox();
            this.btnTodo = new System.Windows.Forms.Button();
            this.btnNada = new System.Windows.Forms.Button();
            this.btnHasse = new System.Windows.Forms.Button();
            this.txtConjuntos = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(104, 152);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(75, 23);
            this.btnGenerar.TabIndex = 0;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // txtbxDesde
            // 
            this.txtbxDesde.Location = new System.Drawing.Point(72, 22);
            this.txtbxDesde.Name = "txtbxDesde";
            this.txtbxDesde.Size = new System.Drawing.Size(150, 20);
            this.txtbxDesde.TabIndex = 1;
            // 
            // txtbxHasta
            // 
            this.txtbxHasta.Location = new System.Drawing.Point(72, 58);
            this.txtbxHasta.Name = "txtbxHasta";
            this.txtbxHasta.Size = new System.Drawing.Size(150, 20);
            this.txtbxHasta.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Desde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hasta";
            // 
            // txtbxNumero
            // 
            this.txtbxNumero.Location = new System.Drawing.Point(72, 126);
            this.txtbxNumero.Name = "txtbxNumero";
            this.txtbxNumero.Size = new System.Drawing.Size(150, 20);
            this.txtbxNumero.TabIndex = 1;
            // 
            // btnRandom
            // 
            this.btnRandom.Location = new System.Drawing.Point(104, 84);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(75, 23);
            this.btnRandom.TabIndex = 0;
            this.btnRandom.Text = "Random";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Numero";
            // 
            // lstbxLista
            // 
            this.lstbxLista.FormattingEnabled = true;
            this.lstbxLista.Location = new System.Drawing.Point(375, 12);
            this.lstbxLista.Name = "lstbxLista";
            this.lstbxLista.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstbxLista.Size = new System.Drawing.Size(120, 238);
            this.lstbxLista.TabIndex = 3;
            // 
            // btnTodo
            // 
            this.btnTodo.Location = new System.Drawing.Point(258, 12);
            this.btnTodo.Name = "btnTodo";
            this.btnTodo.Size = new System.Drawing.Size(111, 23);
            this.btnTodo.TabIndex = 4;
            this.btnTodo.Text = "Seleccionar Todo";
            this.btnTodo.UseVisualStyleBackColor = true;
            this.btnTodo.Click += new System.EventHandler(this.btnTodo_Click);
            // 
            // btnNada
            // 
            this.btnNada.Location = new System.Drawing.Point(258, 51);
            this.btnNada.Name = "btnNada";
            this.btnNada.Size = new System.Drawing.Size(111, 23);
            this.btnNada.TabIndex = 4;
            this.btnNada.Text = "Deseleccionar Todo";
            this.btnNada.UseVisualStyleBackColor = true;
            this.btnNada.Click += new System.EventHandler(this.btnNada_Click);
            // 
            // btnHasse
            // 
            this.btnHasse.Location = new System.Drawing.Point(391, 256);
            this.btnHasse.Name = "btnHasse";
            this.btnHasse.Size = new System.Drawing.Size(93, 23);
            this.btnHasse.TabIndex = 5;
            this.btnHasse.Text = "Generar Hasse";
            this.btnHasse.UseVisualStyleBackColor = true;
            this.btnHasse.Click += new System.EventHandler(this.btnHasse_Click);
            // 
            // txtConjuntos
            // 
            this.txtConjuntos.Location = new System.Drawing.Point(21, 230);
            this.txtConjuntos.Margin = new System.Windows.Forms.Padding(2, 20, 2, 20);
            this.txtConjuntos.Name = "txtConjuntos";
            this.txtConjuntos.ReadOnly = true;
            this.txtConjuntos.Size = new System.Drawing.Size(304, 20);
            this.txtConjuntos.TabIndex = 6;
            this.txtConjuntos.Text = "(NOTACIÓN DE CONJUNTOS)";
            this.txtConjuntos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 289);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.txtbxNumero);
            this.Controls.Add(this.txtConjuntos);
            this.Controls.Add(this.btnHasse);
            this.Controls.Add(this.btnNada);
            this.Controls.Add(this.btnTodo);
            this.Controls.Add(this.lstbxLista);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbxHasta);
            this.Controls.Add(this.txtbxDesde);
            this.Controls.Add(this.btnRandom);
            this.Name = "Control";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.TextBox txtbxDesde;
        private System.Windows.Forms.TextBox txtbxHasta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbxNumero;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstbxLista;
        private System.Windows.Forms.Button btnTodo;
        private System.Windows.Forms.Button btnNada;
        private System.Windows.Forms.Button btnHasse;
        private System.Windows.Forms.TextBox txtConjuntos;
    }
}

