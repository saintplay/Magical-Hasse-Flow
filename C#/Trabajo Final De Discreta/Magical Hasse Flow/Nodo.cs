﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magical_Hasse_Flow
{
    public class Nodo
    {
        public delegate void delegadoNodoIntInt(Nodo pNodo, int posX, int posY);
        public delegadoNodoIntInt onMovimientoLabel { get; set; }
        public int valor {get; set;}
        public int nivel { get; set; }
        public int posicion { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int xScreen { get; set; }
        public int yScreen { get; set; }
        public int xDiff { get; set; }
        public int yDiff { get; set; }
        public int colorR { get; set; }
        public int colorG { get; set; }
        public int colorB { get; set; }
        public Label label { get; set; }
        private bool mouseClick;
        public Nodo(int pValor)
        {
            label = new Label();
            label.AutoSize = true;
            label.TabIndex = 2;
            label.ForeColor = System.Drawing.Color.Blue;
            label.Font = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold);
            label.Visible = false;
            label.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_MouseDown);
            label.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label_MouseMove);
            label.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label_MouseUp);
            mouseClick = false;
            xDiff = 0;
            yDiff = 0;
            valor = pValor;
            nivel = -1;

            label.Text = valor.ToString();
        }

        private void label_MouseDown(object sender, MouseEventArgs e)
        {
            mouseClick = true;
        }
        private void label_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && mouseClick)
            {
                onMovimientoLabel(this, label.Location.X +  e.X, label.Location.Y + e.Y);
            }
        }
        private void label_MouseUp(object sender, MouseEventArgs e)
        {
            mouseClick = false;
        }
        
    }

}