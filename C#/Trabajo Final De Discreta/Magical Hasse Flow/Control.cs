﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magical_Hasse_Flow
{
    public partial class Control : Form
    {
        public Control()
        {           
            InitializeComponent();
        }

        private void btnRandom_Click(object sender, EventArgs e)
        {
            Random aleatorio = new Random();
            int random = aleatorio.Next(Convert.ToInt32(txtbxDesde.Text), Convert.ToInt32(txtbxHasta.Text) + 1);
            txtbxNumero.Text = random.ToString();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(txtbxNumero.Text);

            lstbxLista.Items.Clear();

            for (int i = 1; i <= (n + 1) / 2; i++)
            {
                if (n % i == 0)
                {
                    lstbxLista.Items.Add(i.ToString());
                }
            }
            lstbxLista.Items.Add(n.ToString());

            for (int i = 0; i < lstbxLista.Items.Count; i++)
            {
                lstbxLista.SetSelected(i, true);
            }
            
        }

        private void btnTodo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstbxLista.Items.Count; i++)
            {
                lstbxLista.SetSelected(i, true);
            }
        }

        private void btnNada_Click(object sender, EventArgs e)
        {
            lstbxLista.ClearSelected();
        }

        private void btnHasse_Click(object sender, EventArgs e)
        {
            Hasse2D hasse = new Hasse2D();

            hasse.dominio = new List<Nodo>();
            Nodo nodo;

            for (int i = 0; i < lstbxLista.Items.Count; i++)
            {
                if (lstbxLista.GetSelected(i))
                {
                    nodo = new Nodo(Convert.ToInt32(lstbxLista.Items[i]));
                    hasse.dominio.Add(nodo);
                }
            }

            hasse.goHasse();
            txtConjuntos.Text = hasse.NotacionDeConjuntos();

            Grafico grafico = new Grafico(hasse);

            if (hasse.antisimetricas.Count != 0)
            {
                grafico.hasse = hasse;
                grafico.Show();
                grafico.DibujarHasse(hasse, grafico.buffer.Graphics);
                grafico.buffer.Render(grafico.graphics);
                grafico.Flow.Enabled = true;
            }
            else
                MessageBox.Show("La relacion no forma un Hasse");
        }
    }
}
